(docker rm -f dfg-hacka-server || true) && docker run -d --rm -d --name dfg-hacka-server --env-file ./.env -w /app \
--net=caddy-node_caddy --net-alias=dfg-hacka-server \
-v "$(pwd)/bin:/app/bin" \
-v "$(pwd)/.env:/app/.env" \
-v "$(pwd)/helpers:/app/helpers" \
-v "$(pwd)/models:/app/models" \
-v "$(pwd)/public:/app/public" \
-v "$(pwd)/routes:/app/routes" \
-v "$(pwd)/app.js:/app/app.js" \
-v "$(pwd)/package.json:/app/package.json" \
-v "$(pwd)/package.lock.json:/app/package.lock.json" \
-v "$(pwd)/entry.sh:/app/entry.sh" \
-v "/root/.npm/_cacache:/root/.npm/_cacache" node:13.5.0-alpine sh entry.sh \