const express = require("express");
const router = express.Router();
const createError = require("http-errors");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const User = require("../models/user");

var info = require("debug")(
  "app:INFO:auth"
);

// HELPER FUNCTIONS
const {
  isLoggedIn,
  isNotLoggedIn,
  validationLogin
} = require("../helpers/middlewares");

// POST '/auth/signup'
router.post('/signup', validationLogin, isNotLoggedIn, async (req, res, next) => {
  var { firstName, lastName, email, password, location, skills, preferedProject } = req.body;
  preferedProject = preferedProject || ["NGO"]
  skills = skills || ["WebDev"]
  try {																									 // projection
    const emailExists = await User.findOne({ email }, 'email');           // limit response to 'email'

    if (emailExists) return next(createError(400));
    else {

      const salt = bcrypt.genSaltSync(saltRounds);
      const hashPass = bcrypt.hashSync(password, salt);
      const newUser = await User.create({ firstName, lastName, email, location, skills, preferedProject, password: hashPass });

      newUser.password = "*";
      req.session.currentUser = newUser;
      res
        .status(201)  //  Created
        .json(newUser);
    }
  }
  catch (error) {
    next(createError(error));
  }
},
);

// POST '/auth/password_reset'
router.post('/password_reset', isNotLoggedIn, async (req, res, next) => {
  var { email } = req.body;
  try {
    const user = await User.findOne({ email });
    if (!user) {
      info(JSON.stringify({
        type:"password_reset_user_not_found",
        email
      }))
      res
        .status(201)  //  OK
        .send();
    }
    else {
      const {hash, password} = require('../helpers/password')();
      user.password = hash

     

      let mailgun_response = await require('../helpers/mailgun')({
        from: "DEV4GOOD <no-reply@misitioba.com>",
        to:email,
        subject:"Password reset",
        text:`Your password has been reset: ${password}
        `
      })

      info(JSON.stringify({
        type:"password_reset_success",
        email,
        metadata:{
          mailgun_response
        }
      }))

      await user.save()
      res
        .status(201)  //  OK
        .send();
    }
  }
  catch (error) {
    next(createError(error));
  }
},
);

// POST '/auth/login'
router.post('/login', isNotLoggedIn, validationLogin, async (req, res, next) => {
  const { email, password } = req.body;
  try {

    const user = await User.findOne({ email });
    if (!user) {
      next(createError(404));
    }
    else if (bcrypt.compareSync(password, user.password)) {

      user.password = '*';
      req.session.currentUser = user;
      res
        .status(200)
        .json(user);

    }
    else {
      next(createError(401));	// Unauthorized
    }
  }
  catch (error) {
    next(createError(error));
  }
},
);

// POST '/auth/logout'
router.post('/logout', isLoggedIn, (req, res, next) => {
  req.session.destroy();
  res
    .status(204).json({ "message": "your logged out" })  //  No Content
    .send();
});

// GET '/auth/me'
router.get('/me', isLoggedIn, (req, res, next) => {
  const currentUserSessionData = req.session.currentUser;
  currentUserSessionData.password = '*';
  console.log('req.session.currentUser._id', req.session.currentUser._id)
  res.status(200).json(currentUserSessionData);
});

module.exports = router;
